﻿using System;
using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MusicClassificationByGenre.ViewModels;
using MusicClassificationByGenre.Views;

namespace MusicClassificationByGenre.Test
{
	[TestClass]
	public class ViewTests
	{
		private MainViewModel viewModel;
		private MainWindow window;

		[STAThread]
		[TestInitialize]
		public void TestInitialize()
		{
			window = new MainWindow();
			viewModel = (MainViewModel)window.DataContext;
			window.Show();
		}

		[STAThread]
		[TestMethod]
		public void TestShowBusyIndicator()
		{
			//arrange
			var progressRingTextBox = window.FindChild<TextBox>("ProgressRingTextBox");
			var progressRing = window.FindChild<ProgressRing>("ProgressRing");

			//act
			viewModel.IsBusy = true;
			viewModel.BusyContent = "TestContent";

			//assert
			Assert.IsTrue(progressRing.IsActive);
			Assert.AreEqual(progressRingTextBox.Text, "TestContent");
		}

		[STAThread]
		[TestMethod]
		public void TestHideBusyIndicatorWhileIsBusySetToFalse()
		{
			//arrange
			var progressRingTextBox = window.FindChild<TextBox>("ProgressRingTextBox");
			var progressRing = window.FindChild<ProgressRing>("ProgressRing");

			//act
			viewModel.IsBusy = true;
			viewModel.BusyContent = "TestContent";
			viewModel.IsBusy = false;

			//assert
			Assert.IsFalse(progressRing.IsActive);
			Assert.IsTrue(progressRingTextBox.Visibility == Visibility.Collapsed);
			Assert.IsTrue(string.IsNullOrEmpty(progressRingTextBox.Text));
		}

		[STAThread]
		[TestMethod]
		public void TestTextBindings()
		{
			//arrange
			var recordFilePathTextBox = window.FindChild<TextBox>("RecordFilePathTextBox");
			var dataSetFilePathTextBox = window.FindChild<TextBox>("DataSetFilePathTextBox");

			//act
			viewModel.TestDataFilePath = "TestRecordFilePathBinding";
			viewModel.DataSetFilePath = "TestDataSetFilePathBinding";

			//assert
			Assert.AreEqual("TestRecordFilePathBinding", recordFilePathTextBox.Text);
			Assert.AreEqual("TestDataSetFilePathBinding", dataSetFilePathTextBox.Text);
		}

		[STAThread]
		[TestMethod]
		public void TestMessages()
		{
			//arrange
			var messagesListBox = window.FindChild<ListBox>("MessagesListBox");

			//act
			//viewModel.Messages.Add("Test message");

			//assert
			Assert.AreEqual(1, messagesListBox.Items.Count);
			Assert.AreEqual("Test message", messagesListBox.Items[0].ToString());
		}

		[TestCleanup]
		public void Cleanup()
		{
			window.Close();
			window = null;
			viewModel = null;
		}
	}
}