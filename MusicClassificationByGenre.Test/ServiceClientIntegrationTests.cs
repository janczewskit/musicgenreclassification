﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MusicClassificationByGenre.Models;
using MusicClassificationByGenre.RestClient;

namespace MusicClassificationByGenre.Test
{
	[TestClass]
	public class ServiceClientIntegrationTests
	{
		private ServiceClient client;

		[TestInitialize]
		public void Initialize()
		{
			client = new ServiceClient();
		}

		[TestMethod]
		public void TestConnection()
		{
			bool result = client.TestConnection();
			Assert.IsTrue(result);
		}

		[TestMethod]
		public void TestTrainingData()
		{
			string trainingSet = Resources.test_training_set;
			MusicClassificationResponse result = client.TrainNetwork(trainingSet);
			Assert.AreEqual(int.Parse(Resources.ResponseOk), result.requestStatus);
		}

		[TestMethod]
		public void TestClassification()
		{
			string trainingSet = Resources.test_training_set;
			client.TrainNetwork(trainingSet);
			var result = client.Classify(trainingSet);
			Assert.AreEqual(int.Parse(Resources.ResponseOk), result.requestStatus);
			Assert.IsTrue(result.content.Length > 0);
		}

		[TestCleanup]
		public void Cleanup()
		{
			client.Dispose();
		}
	}
}