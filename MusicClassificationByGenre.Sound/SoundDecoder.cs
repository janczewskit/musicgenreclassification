﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using Accord.Audio;
using Accord.Audio.Formats;
using Accord.Audio.Windows;
using Accord.Math;
using Tools = Accord.Audio.Tools;

namespace MusicClassificationByGenre.Sound
{
	public class SoundDecoder
	{
		private readonly string fileName;

		public SoundDecoder(string fileName)
		{
			this.fileName = fileName;
		}

		public IEnumerable<float> Amplitudes()
		{
			var sourceDecoder = new WaveDecoder(fileName);
			Signal sourceSignal = sourceDecoder.Decode();
			for (int i = 0; i < sourceSignal.Samples; i++)
				yield return sourceSignal.GetSample(1, i);
		}

		public void GetSignalFeatures()
		{
			var sourceDecoder = new WaveDecoder(fileName);
			Signal sourceSignal = sourceDecoder.Decode();

			var window = RaisedCosineWindow.Hamming(1024);
			Signal[] windows = sourceSignal.Split(window, 512);
			ComplexSignal[] complexSignals = windows.Apply(ComplexSignal.FromSignal);

			complexSignals.ForwardFourierTransform();

			foreach (ComplexSignal complexSignal in complexSignals)
			{
				Complex[] channel = complexSignal.GetChannel(0);

				double[] power = Tools.GetPowerSpectrum(channel);
				double[] frequency = Tools.GetFrequencyVector(complexSignal.Length, complexSignal.SampleRate);
				var samplingRate = complexSignal.SampleRate;

				var rms = Tools.RootMeanSquare(GetSamples(complexSignal).ToArray());
			}
		}

		public IEnumerable<float> GetSamples(ComplexSignal signal)
		{
			for (int i = 0; i < signal.Samples; i++)
				yield return signal.GetSample(1, i);
		}

		// tempo(bpm)  rms(dB) sampling frequency(kHz) sampling rate(b)
		// dynamic range(dB)   tonality number of digital errors
		public void GetSignalFeatures2()
		{
			// dynamic range (dB) & time
			using (var reader0 = new NAudio.Wave.WaveFileReader(fileName))
			{
				var time = reader0.TotalTime;
				var bitrate = reader0.WaveFormat.AverageBytesPerSecond * 8;
				Console.WriteLine(bitrate);
			}

			byte[] bytes = File.ReadAllBytes(fileName);
			var stream = new MemoryStream(bytes);
			var reader = new BinaryReader(stream);
			int chunkID = reader.ReadInt32();
			int fileSize = reader.ReadInt32();
			int riffType = reader.ReadInt32();
			int fmtID = reader.ReadInt32();
			int fmtSize = reader.ReadInt32();
			int fmtCode = reader.ReadInt16();
			int channels = reader.ReadInt16();
			int sampleRate = reader.ReadInt32();
			int fmtAvgBPS = reader.ReadInt32();
			int fmtBlockAlign = reader.ReadInt16();
			int bitDepth = reader.ReadInt16();

			Console.WriteLine(sampleRate);

			double sum = 0;
			for (var i = 0; i < _buffer.length; i = i + 2)
			{
				double sample = BitConverter.ToInt16(_buffer, i) / 32768.0;
				sum += (sample * sample);
			}
			double rms = Math.Sqrt(sum / (_buffer.length / 2));
			var decibel = 20 * System.Math.Log10(rms);
		}

		private double GoertzelFilter(float[] samples, double freq, int start, int end)
		{
			// tmp
			double SIGNAL_SAMPLE_RATE = 0;

			double sPrev = 0.0;
			double sPrev2 = 0.0;
			int i;
			double normalizedfreq = freq / SIGNAL_SAMPLE_RATE;
			double coeff = 2 * System.Math.Cos(2 * System.Math.PI * normalizedfreq);
			for (i = start; i < end; i++)
			{
				double s = samples[i] + coeff * sPrev - sPrev2;
				sPrev2 = sPrev;
				sPrev = s;
			}
			double power = sPrev2 * sPrev2 + sPrev * sPrev - coeff * sPrev * sPrev2;
			return power;
		}
	}
}