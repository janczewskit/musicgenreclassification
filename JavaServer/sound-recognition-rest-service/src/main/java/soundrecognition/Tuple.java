package soundrecognition;

public class Tuple<Input, Output> { 
	  public final Input input; 
	  public final Output output; 
	  public Tuple(Input input, Output output) { 
	    this.input = input; 
	    this.output = output;
	  } 
}
