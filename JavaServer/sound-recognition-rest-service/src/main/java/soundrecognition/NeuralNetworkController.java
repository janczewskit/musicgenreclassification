package soundrecognition;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NeuralNetworkController {

    @RequestMapping("/connect")
    public int connect() {
        return 200;
    }

    @SuppressWarnings("finally")
	@RequestMapping("/train")
    public MusicGenreResponse train(@RequestParam(value="set") String set) {
    	int responseNumberValue;
		try {
			MusicClassificationbByGenre.trainNetwork(set);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			responseNumberValue = 500;
		}
		finally
		{
			responseNumberValue = 200;
			return new MusicGenreResponse(responseNumberValue, null);
		}
		
    }
    @SuppressWarnings("finally")
	@RequestMapping("/classify")
    public MusicGenreResponse classify(@RequestParam(value="set") String set) {
    	int responseNumberValue;
		List<Tuple<String, String>> result = null;
		try {
			result = MusicClassificationbByGenre.classify(set);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			responseNumberValue = 500;
			return new MusicGenreResponse(500, null);
		}
			
		responseNumberValue = 200;
		return new MusicGenreResponse(responseNumberValue, result);
    }
}