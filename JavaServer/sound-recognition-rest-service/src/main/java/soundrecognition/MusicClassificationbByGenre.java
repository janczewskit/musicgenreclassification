package soundrecognition;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.learning.DataSet;
import org.neuroph.core.learning.DataSetRow;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.MomentumBackpropagation;
import org.neuroph.util.TrainingSetImport;
import org.neuroph.util.TransferFunctionType;

public class MusicClassificationbByGenre {

	private static MultiLayerPerceptron multiLayerPerceptron;
	
	public static void trainNetwork(String trainingSetText) throws FileNotFoundException {
    	String trainingSetFileName = "TrainingSet.txt";
    	saveFile(trainingSetFileName, trainingSetText);
    	
        int inputsCount = 8;
        int outputsCount = 4;
        
        System.out.println("Running Sample");
        System.out.println("Using training set " + trainingSetFileName);

        DataSet trainingSet = null;
        try {
            trainingSet = TrainingSetImport.importFromFile(trainingSetFileName, inputsCount, outputsCount, ",");
        } catch (FileNotFoundException ex) {
            System.out.println("File not found!");
        } catch (IOException | NumberFormatException ex) {
            System.out.println("Error reading file or bad number format!");
        }

        System.out.println("Creating neural network");
        multiLayerPerceptron = new MultiLayerPerceptron(TransferFunctionType.SIGMOID, 8, 20, 4);

        MomentumBackpropagation learningRule = (MomentumBackpropagation) multiLayerPerceptron.getLearningRule();
        learningRule.setLearningRate(0.5);
        learningRule.setMomentum(0.8);

        System.out.println("Training neural network...");
        multiLayerPerceptron.learn(trainingSet);
        System.out.println("Done training!");
    }

	public static List<Tuple<String,String>> classify(String data) throws FileNotFoundException {
    	String dataSetFileName = "data.txt";
    	saveFile(dataSetFileName, data);
    	
        int inputsCount = 8;
        int outputsCount = 4;
        
        System.out.println("Using data set " + dataSetFileName);

        DataSet dataSet = null;
        try {
            dataSet = TrainingSetImport.importFromFile(dataSetFileName, inputsCount, outputsCount, ",");
        } catch (FileNotFoundException ex) {
            System.out.println("File not found!");
        } catch (IOException | NumberFormatException ex) {
            System.out.println("Error reading file or bad number format!");
        }

		return testMusicClassification(multiLayerPerceptron, dataSet);
	}
    
	private static void saveFile(String trainingSetFileName, String trainingSetText) throws FileNotFoundException {
    	try(PrintWriter fileWriter = new PrintWriter(trainingSetFileName)){
			fileWriter.print(trainingSetText);
    	}
	}

	public static List<Tuple<String,String>> testMusicClassification(NeuralNetwork neuralNetwork, DataSet dataSet) {
		List<Tuple<String,String>> data = new ArrayList<Tuple<String, String>>();
		for (DataSetRow trainingElement : dataSet.getRows()) {
            neuralNetwork.setInput(trainingElement.getInput());
            neuralNetwork.calculate();
            String[] networkInput = round(trainingElement.getInput());
            String[] networkOutput = round(neuralNetwork.getOutput());
            
            String input = arrayToString(networkInput);
            String output = arrayToString(networkOutput);
            
            System.out.print("Input: " + input);
            System.out.println(" Output: " + output);
            
            data.add(new Tuple<String,String>(input,output));
        }
        
        return data;
    }
	
	private static String[] round(double[] collection)
	{
		String[] result = new String[collection.length];
		DecimalFormat decimalFormat = new DecimalFormat("#.####", new DecimalFormatSymbols(Locale.US));
		for(int i = 0; i < collection.length; i++)
			result[i] = decimalFormat.format(collection[i]);

		return result;
	}
	
	private static String arrayToString(String[] values)
	{
		String result = new String();
		for(int i = 0; i < values.length; i++)
		{
			if(i < values.length - 1)
				result += values[i] + ",";
			else
				result += values[i];
		}
		
		return result;
	}
}