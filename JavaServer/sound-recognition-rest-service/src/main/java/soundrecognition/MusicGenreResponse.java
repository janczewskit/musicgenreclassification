package soundrecognition;

import java.util.List;

public class MusicGenreResponse {

    private final int requestStatus;
    private final List<Tuple<String,String>> content;

    public MusicGenreResponse(int requestStatus, List<Tuple<String,String>> content) {
        this.requestStatus = requestStatus;
        this.content = content;
    }

    public int getRequestStatus() {
        return requestStatus;
    }

    public List<Tuple<String,String>> getContent() {
        return content;
    }
}
