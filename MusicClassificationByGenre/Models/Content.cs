﻿using MusicClassificationByGenre.Extensions;

namespace MusicClassificationByGenre.Models
{
	public class Content
	{
		public string input { get; set; }
		public string output { get; set; }

		public override string ToString()
		{
			return $"Input: {input}, output: {output}";
		}

		public int? MaxOutputPositionNumber => output.MaxValuePosition();
	}
}