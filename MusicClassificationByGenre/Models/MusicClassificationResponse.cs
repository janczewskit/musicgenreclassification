﻿namespace MusicClassificationByGenre.Models
{
	public class MusicClassificationResponse
	{
		public int requestStatus { get; set; }
		public Content[] content { get; set; }
	}
}