﻿using System.Windows;
using MahApps.Metro.Controls.Dialogs;
using MusicClassificationByGenre.ViewModels;

namespace MusicClassificationByGenre.Views
{
	public partial class MainWindow
	{
		public MainWindow()
		{
			InitializeComponent();
			DataContext = new MainViewModel(DialogCoordinator.Instance);
		}
	}
}
