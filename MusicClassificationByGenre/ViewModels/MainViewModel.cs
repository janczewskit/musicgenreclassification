﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Win32;
using MusicClassificationByGenre.Extensions;
using MusicClassificationByGenre.Models;
using MusicClassificationByGenre.RestClient;
using Prism.Commands;
using Prism.Mvvm;

namespace MusicClassificationByGenre.ViewModels
{
	public class MainViewModel : BindableBase
	{
		private string dataSetFilePath;

		public string DataSetFilePath
		{
			get { return dataSetFilePath; }
			set
			{
				dataSetFilePath = value;
				LearnCommand.RaiseCanExecuteChanged();
				OnPropertyChanged();
			}
		}

		private string testDataFilePath;

		public string TestDataFilePath
		{
			get { return testDataFilePath; }
			set
			{
				testDataFilePath = value;
				OnPropertyChanged();
			}
		}

		private bool isBusy;

		public bool IsBusy
		{
			get { return isBusy; }
			set
			{
				if (!value)
					BusyContent = string.Empty;

				isBusy = value;
				OnPropertyChanged();
			}
		}

		private string busyContent;

		public string BusyContent
		{
			get { return busyContent; }
			set
			{
				busyContent = value;
				OnPropertyChanged();
			}
		}

		private AggregatedClassificationResult messages;

		public AggregatedClassificationResult Messages
		{
			get { return messages; }
			set
			{
				messages = value;
				OnPropertyChanged();
			}
		}

		private bool canTesting;

		public bool CanTesting
		{
			get { return canTesting; }
			set
			{
				canTesting = value;
				TestingCommand.RaiseCanExecuteChanged();
				OnPropertyChanged();
			}
		}

		public ObservableCollection<TestClass> Results { get; set; }

		private ObservableCollection<SeriesData> series;

		public ObservableCollection<SeriesData> Series
		{
			get { return series; }
			set
			{
				series = value;
				OnPropertyChanged();
				IsChartVisible = series != null && series.Count > 0;
			}
		}

		private bool isChartVisible;

		public bool IsChartVisible
		{
			get { return isChartVisible; }
			set
			{
				isChartVisible = value;
				OnPropertyChanged();
			}
		}

		public DelegateCommand LoadTestSetCommand { get; set; }
		public DelegateCommand TestingCommand { get; set; }
		public DelegateCommand LearnCommand { get; set; }
		public DelegateCommand LoadDataToTestCommand { get; set; }

		private readonly IDialogCoordinator dialogCoordinator;
		private readonly ServiceClient serviceClient;

		public MainViewModel(IDialogCoordinator dialogCoordinator)
		{
			this.dialogCoordinator = dialogCoordinator;
			Messages = new AggregatedClassificationResult();

			try
			{
				serviceClient = new ServiceClient();
				serviceClient.TestConnection();
			}
			catch (Exception ex)
			{
				string exceptionMessage = ex.InnerException?.Message ?? ex.Message;
				dialogCoordinator.ShowMessageAsync(this, Resources.Error,
					$"{Resources.ConnectionErrorAndExceptionMessage} {exceptionMessage}");
				Environment.Exit(0);
			}

			LoadTestSetCommand = new DelegateCommand(OnLoadTestSet);
			LearnCommand = new DelegateCommand(OnLearn, () => DataSetFilePath != null);
			TestingCommand = new DelegateCommand(OnTesting, () => CanTesting);
			LoadDataToTestCommand = new DelegateCommand(OnLoadTestData);

			Results = new ObservableCollection<TestClass>
			{
				new TestClass {Category = "Globalization", Number = 34},
				new TestClass {Category = "Features", Number = 23},
				new TestClass {Category = "ContentTypes", Number = 15},
				new TestClass {Category = "Correctness", Number = 66},
				new TestClass {Category = "Naming", Number = 56},
				new TestClass {Category = "Best Practices", Number = 34}
			};

			//Series = new ObservableCollection<SeriesData>
			//{
			//	//new SeriesData {SeriesDisplayName = "Warnings", Items = Results}
			//};
		}

		private void OnLoadTestSet()
		{
			var openFileDialog = new OpenFileDialog
			{
				Filter = "Training set Files|*.txt",
				CheckFileExists = true,
				Multiselect = false,
				CheckPathExists = true
			};

			if (openFileDialog.ShowDialog() != true)
				return;

			DataSetFilePath = openFileDialog.FileName;
		}

		private void OnLoadTestData()
		{
			var openFileDialog = new OpenFileDialog
			{
				Filter = "Data to proceed File|*.txt",
				CheckFileExists = true,
				Multiselect = false,
				CheckPathExists = true
			};

			if (openFileDialog.ShowDialog() != true)
				return;

			TestDataFilePath = openFileDialog.FileName;
		}

		private void OnLearn()
		{
			if (string.IsNullOrEmpty(DataSetFilePath))
			{
				dialogCoordinator.ShowMessageAsync(this, Resources.Warning, Resources.FirstYouNeedToChooseTheTestKit);
				return;
			}

			OnNeuralNetwokProcess(Learn, Resources.LearningNeuralNetwork);
		}

		private void OnTesting()
		{
			if (string.IsNullOrEmpty(TestDataFilePath))
			{
				dialogCoordinator.ShowMessageAsync(this, Resources.Warning, Resources.YouMustLoadRecordFirst);
				return;
			}

			if (!CanTesting)
			{
				dialogCoordinator.ShowMessageAsync(this, Resources.Warning, Resources.YouMustLearnNetworkFirst);
				return;
			}

			OnNeuralNetwokProcess(Clasify, Resources.TestingNeuralNetwork);
		}

		private void OnNeuralNetwokProcess(Func<Task> neuralNetworkProcessFunction, string busyMessage)
		{
			IsBusy = true;
			BusyContent = busyMessage;
			Task task = neuralNetworkProcessFunction();
			task.ContinueWith(x =>
			{
				if (x.Exception != null)
					dialogCoordinator.ShowMessageAsync(this, Resources.Error, x.Exception.Message);

				IsBusy = false;
			}, TaskContinuationOptions.OnlyOnFaulted | TaskContinuationOptions.ExecuteSynchronously)
				.ContinueWith(x =>
				{
					IsBusy = false;
				}, TaskContinuationOptions.ExecuteSynchronously);
		}

		private Task Learn()
		{
			return Task.Run(() =>
			{
				string trainingSet = File.ReadAllText(DataSetFilePath);
				return serviceClient.TrainNetwork(trainingSet);
			}).ContinueWith(musicClassificationResponse =>
			{
				MusicClassificationResponse result = musicClassificationResponse.Result;
				if (result.requestStatus != int.Parse(Resources.ResponseOk))
					dialogCoordinator.ShowMessageAsync(this, Resources.Error, Resources.ErrorWhileTraningNeuralNetworks);
				else
					CanTesting = true;
			}, TaskScheduler.FromCurrentSynchronizationContext());
		}

		private Task Clasify()
		{
			string[] testData = File.ReadAllLines(TestDataFilePath);
			string mergedTestData = testData.Merge();
			return Task.Run(() => serviceClient.Classify(mergedTestData))
				.ContinueWith(musicClassificationResponse =>
				{
					MusicClassificationResponse result = musicClassificationResponse.Result;
					if (result.requestStatus != int.Parse(Resources.ResponseOk))
					{
						dialogCoordinator.ShowMessageAsync(this, Resources.Error, Resources.ErrorWhileClassifyingRecord);
						return;
					}

					CompareResultsWithExpectedValues(testData, result.content.ToList());
				}, TaskScheduler.FromCurrentSynchronizationContext());
		}

		private void CompareResultsWithExpectedValues(string[] testData, List<Content> results)
		{
			Messages = new AggregatedClassificationResult();
			Assert.AreEqual(results.Count, testData.Length);

			for (int i = 0; i < testData.Length; i++)
			{
				string expectedOutput = testData[i].Remove(0, results[i].input.Length + 1);
				int? maxOutputPositionNumber = results[i].MaxOutputPositionNumber;
				bool arePositionsEquals = maxOutputPositionNumber == expectedOutput.MaxValuePosition();
				Messages.Add(new ClassificationResult(arePositionsEquals, maxOutputPositionNumber ?? 0));
			}

			Series = new ObservableCollection<SeriesData>
			{
				new SeriesData
				{
					Items = Messages.GetResult(),
					SeriesDisplayName = "Procent zgodności"
				}
			};
		}
	}

	public class AggregatedClassificationResult
	{
		private readonly List<ClassificationResult> partialResults;

		public AggregatedClassificationResult()
		{
			partialResults = new List<ClassificationResult>();
		}

		public void Add(ClassificationResult classification) => partialResults.Add(classification);

		public ObservableCollection<TestClass> GetResult()
		{
			Dictionary<int, List<bool>> aggreagetedPartialResults = partialResults
				.GroupBy(x => x.Type, x => x.Approve)
				.ToDictionary(x => x.Key, x => x.Select(p => p).ToList());

			return new ObservableCollection<TestClass>(aggreagetedPartialResults.Select(partialResult
				=> new TestClass
				{
					Category = CategoryDescription(partialResult.Key),
					Number = (partialResult.Value.Count(x => x) / partialResult.Value.Count) * 100
				}));
		}

		private string CategoryDescription(int number)
		{
			MusicGenre musicGenre = (MusicGenre)Enum.Parse(typeof(MusicGenre), number.ToString());
			switch (musicGenre)
			{
				case MusicGenre.Classic:
					return "Klasyczna";

				case MusicGenre.Jazz:
					return "Jazz";

				case MusicGenre.Metal:
					return "Metal";

				case MusicGenre.Pop:
					return "Pop";

				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private enum MusicGenre
		{
			Classic = 0,
			Jazz = 1,
			Metal = 2,
			Pop = 3
		}
	}
}