﻿using System;
using System.Net.Http;
using MusicClassificationByGenre.Models;

namespace MusicClassificationByGenre.RestClient
{
	public class ServiceClient : IDisposable
	{
		private readonly TimeSpan timeout = new TimeSpan(0, 0, 2, 0);
		private const string serviceAddress = "http://localhost:8080";
		private HttpClient client;

		public ServiceClient()
		{
			InitClient();
		}

		private void InitClient()
		{
			var clientHandler = new HttpClientHandler
			{
				UseDefaultCredentials = true,
				PreAuthenticate = true,
				ClientCertificateOptions = ClientCertificateOption.Automatic
			};

			client = new HttpClient(clientHandler)
			{
				Timeout = timeout
			};
		}

		public bool TestConnection()
		{
			string command = $"{serviceAddress}/connect";
			string response = client.GetAsync(command).ReadAsyncAsString();
			return response == Resources.ResponseOk;
		}

		public MusicClassificationResponse TrainNetwork(string trainingSet)
		{
			string command = $"{serviceAddress}/train?set={trainingSet}";
			return client.GetAsync(command).ReadAsync<MusicClassificationResponse>();
		}

		public MusicClassificationResponse Classify(string dataSet)
		{
			string command = $"{serviceAddress}/classify?set={dataSet}";
			return client.GetAsync(command).ReadAsync<MusicClassificationResponse>();
		}

		~ServiceClient()
		{
			Dispose();
		}

		public void Dispose()
		{
			client.Dispose();
			GC.SuppressFinalize(this);
		}
	}
}