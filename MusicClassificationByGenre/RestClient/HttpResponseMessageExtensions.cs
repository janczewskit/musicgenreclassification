﻿using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MusicClassificationByGenre.RestClient
{
	public static class HttpResponseMessageExtensions
	{
		public static string ReadAsyncAsString(this Task<HttpResponseMessage> responseMessage)
		{
			return responseMessage.Result.Content.ReadAsStringAsync().Result;
		}

		public static TModel ReadAsync<TModel>(this Task<HttpResponseMessage> taskResponseMessage) where TModel : class
		{
			TModel data = null;
			var task = taskResponseMessage.ContinueWith(taskWithResponse =>
			{
				HttpResponseMessage responseMessage = taskWithResponse.Result;
				Task<string> response = responseMessage.Content.ReadAsStringAsync();
				response.Wait();
				data = JsonConvert.DeserializeObject<TModel>(response.Result);
			});
			task.Wait();

			return data;
		}
	}
}
